-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2017 at 06:47 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `redjo`
--

-- --------------------------------------------------------

--
-- Table structure for table `catagory`
--

CREATE TABLE IF NOT EXISTS `catagory` (
  `cata_id` int(11) NOT NULL,
  `cata_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`cata_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catagory`
--

INSERT INTO `catagory` (`cata_id`, `cata_name`) VALUES
(10, 'Food'),
(11, 'Dress'),
(12, 'Cosmetics'),
(13, 'Ladies Products'),
(14, 'Gents Products'),
(15, 'Kids Products');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `cus_id` int(11) NOT NULL,
  `cus_name` varchar(150) DEFAULT NULL,
  `cus_phone` varchar(150) DEFAULT NULL,
  `cus_address` varchar(150) DEFAULT NULL,
  `membership_date` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`cus_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cus_id`, `cus_name`, `cus_phone`, `cus_address`, `membership_date`) VALUES
(10, 'Kamal', '123423', 'Banani', '01-08-15'),
(11, 'Jamal', '725670', 'Gulshan', '04-10-15'),
(12, 'Rahim', '623456', 'Banani', '01-07-16'),
(13, 'Kamal', '123423', 'Banani', '14-08-15'),
(14, 'Jamal', '120975', 'Uttara', '05-08-14'),
(15, 'Nazma', '324750', 'Gulshan', '07-11-14'),
(16, 'Kaniz', '123654', 'Uttara', '05-08-14'),
(17, 'Sima', '122255', 'Mirpur', '18-12-16'),
(18, 'Jemy', '347728', 'Banani', '05-08-15'),
(19, 'Disha', '120937', 'Uttara', '25-02-14'),
(20, 'Mahir', '246789', 'Mirpur', '04-08-16');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(150) DEFAULT NULL,
  `emp_phone` varchar(150) DEFAULT NULL,
  `emp_address` varchar(150) DEFAULT NULL,
  `sec_id` int(11) NOT NULL,
  PRIMARY KEY (`emp_id`),
  KEY `sec_id` (`sec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `emp_name`, `emp_phone`, `emp_address`, `sec_id`) VALUES
(101, 'Mizan', '34567', 'Mirpur', 1),
(102, 'Ramij', '55567', 'Dhanmondi', 2),
(103, 'John', '35767', 'Mirpur', 4),
(104, 'Nasir', '87567', 'Uttara', 1),
(105, 'Rabita', '34578', 'Banani', 3),
(106, 'Tarin', '34599', 'Uttara', 2);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `prod_id` int(11) NOT NULL,
  `prod_name` varchar(150) DEFAULT NULL,
  `prod_price` varchar(150) DEFAULT NULL,
  `cata_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prod_id`),
  KEY `cata_id` (`cata_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prod_id`, `prod_name`, `prod_price`, `cata_id`) VALUES
(20, 'Chocolate', '300', 10),
(21, 'Shirt', '550', 11),
(22, 'Toys', '720', 15),
(23, 'Juice', '180', 10),
(24, 'Fruits', '600', 10),
(25, 'Soap', '100', 12);

-- --------------------------------------------------------

--
-- Table structure for table `product_promotion`
--

CREATE TABLE IF NOT EXISTS `product_promotion` (
  `prom_id` int(11) NOT NULL,
  `prom_details` varchar(150) DEFAULT NULL,
  `prod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prom_id`),
  KEY `prod_id` (`prod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_promotion`
--

INSERT INTO `product_promotion` (`prom_id`, `prom_details`, `prod_id`) VALUES
(201, '10% off', 23),
(202, '5% off', 21),
(203, '12% off', 20),
(204, '10% off', 25),
(205, '15% off', 24);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `pur_id` int(11) NOT NULL,
  `prod_id` int(11) DEFAULT NULL,
  `pur_date` varchar(150) DEFAULT NULL,
  `amount` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`pur_id`),
  KEY `prod_id` (`prod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`pur_id`, `prod_id`, `pur_date`, `amount`) VALUES
(10, 21, '10-08-15', '1'),
(11, 23, '11-07-15', '20'),
(12, 22, '09-03-17', '4'),
(13, 24, '19-03-17', '10'),
(14, 23, '11-07-15', '15'),
(15, 22, '09-02-17', '5'),
(16, 23, '16-07-16', '8'),
(17, 25, '09-01-17', '5');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `sec_id` int(11) NOT NULL,
  `sec_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`sec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`sec_id`, `sec_name`) VALUES
(101, 'Food'),
(102, 'Dress'),
(103, 'Kids Products'),
(104, 'Ladies Products'),
(105, 'Cosmetics');

-- --------------------------------------------------------

--
-- Table structure for table `stack`
--

CREATE TABLE IF NOT EXISTS `stack` (
  `stack_id` int(11) NOT NULL,
  `quantity` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stack`
--

INSERT INTO `stack` (`stack_id`, `quantity`) VALUES
(10, '500'),
(11, '400'),
(12, '350'),
(13, '400'),
(14, '550'),
(15, '600');

-- --------------------------------------------------------

--
-- Table structure for table `supervisor`
--

CREATE TABLE IF NOT EXISTS `supervisor` (
  `sup_id` int(11) NOT NULL,
  `sup_name` varchar(150) DEFAULT NULL,
  `supr_phone` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`sup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisor`
--

INSERT INTO `supervisor` (`sup_id`, `sup_name`, `supr_phone`) VALUES
(101, 'Arman', '67938'),
(102, 'Dipu', '38799'),
(103, 'Mita', '23438'),
(104, 'Dipa', '89499'),
(105, 'Sarmin', '34938'),
(106, 'Saju', '87965'),
(107, 'Karim', '63238'),
(108, 'Runa', '98799');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `supp_id` int(11) NOT NULL,
  `supp_name` varchar(150) DEFAULT NULL,
  `supp_phone` varchar(150) DEFAULT NULL,
  `supp_address` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`supp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supp_id`, `supp_name`, `supp_phone`, `supp_address`) VALUES
(1, 'Monir', '27563', 'Banani'),
(2, 'Jamir', '27233', 'Mohakhali'),
(3, 'Ripon', '38653', 'Banani'),
(4, 'Rahim', '19233', 'Mohakhali'),
(5, 'Kamal', '27599', 'Mirpur'),
(6, 'Tamim', '29873', 'Uttara'),
(7, 'Jalal', '88963', 'Banani'),
(8, 'Karim', '27009', 'Uttara');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
